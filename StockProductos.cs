﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace Examencarrito
{
    public class StockProductos
    {
        public StockProductos()
        {
            this.ListaStockProductos = new List<Producto>();
        }

        public List<Producto> ListaStockProductos { get; set; }

        public void CrearProductos()
        {
            Random random = new Random();
            int numero;
            for (int i = 1; i <= 10; i++)
            {

                Producto producto = new Producto();
                producto.Identificador = i;
                numero = random.Next(20);
                char letra = (char)(((int)'A') + random.Next(26));
                producto.Descripcion = "PRODUCTO" + letra + numero + i;
                producto.Elprecio = numero;
                this.ListaStockProductos.Add(producto);
            }
        }
        // 1)	Modifique el método ImprimirStockProductos() de la clase StockProducto para que,
        // utilizando expresiones lambda, devuelva una colección de datos que permita recibirla en Program.cs
        // y mostrar por pantalla el identificador, descripción, precio y existencia. (1P)
        //Agregue uno o varios screenshot del código fuente modificado y un screenshot del resultado obtenido por consola:
        public void ImprimirStockProductos()
        {
            Console.WriteLine("Stock de Productos");
            Console.WriteLine("Identificador\tDescripción\tPrecio\tExistencia");
            List<string> productito = (from Elproducto in ListaStockProductos
                                       orderby Elproducto.Elprecio ascending
                                       select "\t" + Elproducto.Identificador + "\t" + Elproducto.Descripcion + "\t" 
                                       + Elproducto.Elprecio + "\t" + Elproducto.Existencia).ToList();
            Console.WriteLine(string.Join("\n", productito));

        }
        // 2)	Utilizando Linq o lambda desarrolle un método en la clase llamada StockProducto que permita la búsqueda de un producto
        // en particular de la colección de datos ListaStockProductos y devuelva ese objeto. (1P)
        // Agregue uno o varios screenshot del código fuente modificado y un screenshot del resultado obtenido por consola:

        public void Metodobuscar()
        {
            Console.WriteLine(" Ingrese el codigo ");
            int Codigoqr = int.Parse(Console.ReadLine());
            var Search = from Buscaproducto in ListaStockProductos
                         where Buscaproducto.Identificador == Codigoqr
                         select " El producto a buscar  " + Buscaproducto.Descripcion + " Precio: " + Buscaproducto.Elprecio + " Existencias: " + Buscaproducto.Existencia;
            Console.WriteLine(string.Join("\n", Search));
        }
        // 3)Una vez realizado el punto 1 y utilizando expresiones lambda ordene el listado de los productos por precio de menor a mayor. (1P)
        // 4)	Una vez realizado el punto 1 y utilizando expresiones lambda debe mostrar por pantalla el producto cuyo precio sea el mas alto. (1P)
        public void Preciomayor()
        {
            Console.WriteLine("");
            var Preciomayor = ListaStockProductos.Max(a => a.Elprecio);
            Console.WriteLine(" El precio mayor de los productos es:" + Preciomayor);
        }
       
    }
}