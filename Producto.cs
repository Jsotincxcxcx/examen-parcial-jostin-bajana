﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examencarrito
{
    public class Producto
    {
        public int Identificador { get; set; }

        public string Descripcion { get; set; }

        public decimal Precio { get; set; }
        public int Existencia { get; set; }
        public int Elprecio { get; internal set; }
    }
}
